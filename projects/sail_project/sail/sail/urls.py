from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sail.views.home', name='home'),
    # url(r'^sail/', include('sail.foo.urls')),
    # v1 REST API
    url(r'^api/v1/', include('place_reservations_rest_api_v1.urls')),
    # url(r'^api/v1/', include('place_reservations_rest_api_v1.urls', namespace='v1')),

    # url(r'^auth/', include('sail_login.urls', namespace='sail_login')),
    
    # Frontend app
    url(r'^app/', include('sail_frontend_app.urls', namespace='sail_frontend_app')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
