from datetime import datetime

from moneyed import CURRENCIES, DEFAULT_CURRENCY_CODE

from django.utils.timezone import get_default_timezone
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# List of allowed currencies. Contains currency ISO 4217 code and description.
CURRENCY_CHOICES = [(c.code, c.name) for i, c in CURRENCIES.items() if
                        c.code != DEFAULT_CURRENCY_CODE]

class Place(models.Model):

    # The user who owns the place
    user = models.ForeignKey(User)
    
    # Descriptive name of the place
    name = models.CharField(_('place_name'), max_length=255)
    
    # Description of the place
    description = models.TextField(_('place_description'), blank=True)
    
    # Currency used for reservations
    currency = models.CharField(_('reservations_currency'), max_length=3, blank=False, null=False, choices=CURRENCY_CHOICES)
    
    # When the place was created by the user
    date_created = models.DateTimeField(editable=False)
    
    # When the place was modified by the user
    date_modified = models.DateTimeField(editable=False)

    def __unicode__(self):
        return '{0}'.format(self.name)
    
    def save(self, *args, **kwargs):
        # Update timestamps on save
        
        # Retrieve current time once for consistency
        timezone = get_default_timezone()
        current_time = datetime.now(timezone)
        
        # Set creation date if object was just created
        if not self.id:
            self.date_created = current_time
        
        # Always update modification date
        self.date_modified = current_time
        
        super(Place, self).save(*args, **kwargs)
