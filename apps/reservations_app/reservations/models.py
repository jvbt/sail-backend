from datetime import datetime

from django.utils.timezone import get_default_timezone
from django.db import models
from django.contrib.auth.models import User

class Reservation(models.Model):

    # The user who owns the reservation
    user = models.ForeignKey(User)
    
    # The date where the reservation was created
    date_created = models.DateTimeField(editable=False)
    
    # The date where the reservation was last modified
    date_modified = models.DateTimeField(editable=False)
    
    # The date where the reservation starts
    date_from = models.DateTimeField(null=False, blank=False)
    
    # The date where the reservation ends
    date_to = models.DateTimeField(null=False, blank=False)
    
	# Each subclass should have its own table
    class Meta:
        abstract = True
    
    def __unicode__(self):
        return '{0} - {1} - {2}'.format(self.date_from, self.date_to, self.user)
    
    def save(self, *args, **kwargs):
        # Update timestamps on save
        
        # Retrieve current time once for consistency
        timezone = get_default_timezone()
        current_time = datetime.now(timezone)
        
        # Set creation date if object was just created
        if not self.id:
            self.date_created = current_time
        
        # Always update modification date
        self.date_modified = current_time
        
        super(Reservation, self).save(*args, **kwargs)
