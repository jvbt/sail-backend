from django.contrib import admin
from place_reservations.models import PlaceReservation

admin.site.register(PlaceReservation)
