from django.db import models
from django.utils.translation import ugettext_lazy as _
from reservations.models import Reservation
from places.models import Place

class PlaceReservation(Reservation):

    # Descriptive name of the reservation.
    name = models.CharField(_('reservation_name'), max_length=255)
    
    # Description of the reservation.
    description = models.TextField(_('reservation_description'), blank=True)
    
    # The place associated with the reservation.
    place = models.ForeignKey(Place)
    
    # The price of the reservation. The associated currency is in the Place model.
    # This field has 13 integer digits (15 - 2) and 2 decimals.
    # It is enough to store 1 trillion 10^(13 - 1) and just under 10 trillions.
    price = models.DecimalField(max_digits=15, decimal_places=2, blank=True, default=0)
    
    def __unicode__(self):
        return '{0} - {1}'.format(self.name, self.user)
