"""
Use sail_login as the namespace when including those urls inside another urlpatterns:

    urlpatterns = patterns('',
        ...
        url(r'^auth/', include('sail_login.urls', namespace='sail_login'))
    )
"""

from django.conf.urls import patterns, url

urlpatterns = patterns('django.contrib.auth.views',
    url(r'^login/$', 'login', {'template_name': 'sail_login/login.html', 'redirect_field_name': None}, name='login'),
    url(r'^logout/$', 'logout', {'template_name': 'sail_login/logout.html', 'redirect_field_name':None}, name='logout'),
)
