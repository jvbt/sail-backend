from django.core.exceptions import ValidationError
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_naive
from rest_framework.exceptions import ParseError

import django_filters
from place_reservations.models import PlaceReservation

''' This class is used to filter PlaceReservation instances.
    It was added for the client-side calendar REST requests.'''
class PlaceReservationFilter(django_filters.FilterSet):
    
    # Date_from filters
    min_date_from = django_filters.DateTimeFilter(name="date_from", lookup_type='gte')
    max_date_from = django_filters.DateTimeFilter(name="date_from", lookup_type='lte')
    
    # Date_to filters
    min_date_to = django_filters.DateTimeFilter(name="date_to", lookup_type='gte')
    max_date_to = django_filters.DateTimeFilter(name="date_to", lookup_type='lte')
    
    class Meta:
        model = PlaceReservation
        fields = ['min_date_from', 'max_date_from', 'min_date_to', 'max_date_to']
    
    # Initialization
    def __init__(self, *args, **kwargs):
        if len(args) != 1:
            raise ValidationError('PlaceReservationFilter got more than 1 argument')
        
        # Copy request.QUERY_PARAMS to obtain a mutable dictionary.
        request_QUERY_PARAMS = args[0].copy();
        
        # Date fields we will iterate on.
        date_fields = ['min_date_from', 'max_date_from', 'min_date_to', 'max_date_to']
        
        # Try and convert ISO-8601 UTC dates to native types.
        # Iterate over date fields.
        for field in date_fields:
            date_string = request_QUERY_PARAMS.get(field, None)
            if date_string is None:
                # This date field is not in the query. Skip.
                continue
            
            # Parse_datetime raises ValueError if the input is well-formatted but not a valid datetime.
            # It returns None if the input is not well formatted.
            try:
                date_object = parse_datetime(date_string)
            except ValueError:
                raise ParseError(detail='Parameter {0} is well-formatted but not a valid date-time.'.format(field))
                
            if date_object is None:
                raise ParseError(detail='Parameter {0} is not a well-formatted date-time.'.format(field))
            
            if is_naive(date_object):
                raise ParseError(detail='Parameter {0} is a valid date-time but does not specify a time zone.'.format(field))
            
            # At this point we have a valid time zone-aware datetime object.
            # Update the dictionary.
            request_QUERY_PARAMS.update({field: date_object})
        
        # We assume that the object to update is the first value in the tuple.
        args = (request_QUERY_PARAMS,) + args[1::]
        super(PlaceReservationFilter, self).__init__(*args, **kwargs)
