from django.core.exceptions import ObjectDoesNotExist

from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated

from place_reservations_rest_api_v1.permissions import IsOwner
from place_reservations_rest_api_v1.serializers import PlaceSerializer, PlaceReservationSerializer
from place_reservations_rest_api_v1.filters import PlaceReservationFilter


class PlaceViewSet(viewsets.ModelViewSet):
    serializer_class = PlaceSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        return self.request.user.place_set.all()

    def pre_save(self, obj):
        try:
            obj.user
        except ObjectDoesNotExist:
            obj.user = self.request.user


class PlaceReservationViewSet(viewsets.ModelViewSet):
    serializer_class = PlaceReservationSerializer
    permission_classes = [IsAuthenticated, IsOwner]
    filter_class = PlaceReservationFilter
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        user = self.request.user
        queryset = user.placereservation_set.all()
        return queryset

    def pre_save(self, obj):
        try:
            obj.user
        except ObjectDoesNotExist:
            obj.user = self.request.user
