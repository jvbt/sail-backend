from itertools import chain
from django.db import connection
from django.db.models import Sum
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_naive
from pytz import common_timezones_set
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from places.models import Place
from place_reservations.models import PlaceReservation

def get_postgresql_supported_timezones():
    cursor = connection.cursor()
    
    #
    # We only need the time zone name in this query, and not abbrev.
    # The abbreviation will change depending on the offset at the current time.
    # Using the abbreviation would give incorrect results when processing dates in the past.
    #
    cursor.execute("SELECT name FROM pg_timezone_names")
    
    rows = cursor.fetchall()
    
    cursor.close()

    return rows

#
# Long term, the best option might be to have a separate list of supported
# timezones and verify it against the db and pytz on startup.
#
# We might need such a list for the client anyway.
#

def setup_get_supported_timezones_set():
    rows = get_postgresql_supported_timezones()
    
    #
    # Rows contains a list of tuples with 1 item each, we use chain.from_iterables to flatten the list.
    #
    db_timezones_iterator = chain.from_iterable(rows)
    
    #
    # We could create a frozenset, but it is not actually needed.
    # db_timezones_set = frozenset(timezones_iterator)
    #

    #
    # We expect the set of supported timezones to be very similar to common_timezones_set from pytz.
    # The only expected difference is that it should not have the "Antarctica/Troll" time zone.
    #
    my_supported_timezones_set = common_timezones_set.intersection(db_timezones_iterator)

    #
    # Sanity checks.
    #
    if my_supported_timezones_set is None:
        raise Exception("my_supported_timezones_set is None")
    
    if len(my_supported_timezones_set) == 0:
        raise Exception("my_supported_timezones_set is empty")
    
    if None in my_supported_timezones_set:
        raise Exception("None entry in my_supported_timezones_set")
        
    if '' in my_supported_timezones_set:
        raise Exception("Empty string entry in my_supported_timezones_set")
    
    return my_supported_timezones_set

#
# Creates the set of supported time zones.
# This call can throw on init, on purpose.
#
supported_timezones_set = setup_get_supported_timezones_set()

place_reservation_table_name = PlaceReservation._meta.db_table


#
# This will be tricky with reversion. We will
# have to exclude non-current versions.
#
# The easiest might be to look at the list of tables then
# and decide where to go from there.
#
# If this report is often used, we might want to convert
# the query to a stored procedure.
# And handle uninstall/install on startup.
# 
# place_reservation.date_to > %s::TIMESTAMP WITH TIME ZONE => place_reservation.date_from > %s::TIMESTAMP WITH TIME ZONE
#
report_query_template = """
SELECT date_time, amount, (SUM(amount) OVER (ORDER BY date_time)) AS running_total_amount
FROM
(
    SELECT date_time, SUM(price) AS amount
    FROM
    (
        SELECT (DATE_TRUNC('{date_trunc_field}', date_from AT TIME ZONE %s)) AS date_time, price
        FROM "{place_reservation_table_name}" AS place_reservation
        WHERE
        (
            place_reservation.place_id = %s
            AND place_reservation.user_id = %s
            AND place_reservation.date_from > %s::TIMESTAMP WITH TIME ZONE
            AND place_reservation.date_from < %s::TIMESTAMP WITH TIME ZONE
        )
    )
    AS aggregated_amount_table
    GROUP BY date_time
)
AS running_total_table
ORDER BY date_time ASC
"""

daily_report_query = report_query_template.format(date_trunc_field='day', place_reservation_table_name=place_reservation_table_name)

def get_daily_report_tuples(time_zone, place_id, user_id, date_from, date_to):
    cursor = connection.cursor()

    cursor.execute(daily_report_query, [time_zone, place_id, user_id, date_from, date_to])
    
    rows = cursor.fetchall()
    
    cursor.close()

    return rows

def get_report_queryset(queryset, time_zone):
    #
    # Expectations:
    #   queryset is a PlaceReservation queryset.
    #   time_zone is a string.
    #

    #
    # Create tuple of SQL string, param to obtain truncated date in user time zone.
    #
    truncate_date = connection.ops.datetime_trunc_sql('day', 'date_from', time_zone)
    
    #
    # Add truncated date field for each date.
    #
    queryset = queryset.extra(select={'date_time': truncate_date[0]}, select_params=[truncate_date[1]])

    #
    # Add total_amount annotation.
    # At this point output consists of date (that should be sent as UTC date) and sum_amount per date.
    #
    queryset = queryset.values('date_time').annotate(Sum('price'))
    queryset = queryset.extra(select={'amount': "sum(price)"})

    #
    # Add running total using a window function.
    #
    queryset = queryset.extra(select={'running_total_amount': "sum(price__sum) OVER (ORDER BY date_time)"})


    queryset = queryset.values('date_time', 'price__sum', 'running_total_amount')

    #
    # Order results by date.
    #
    queryset = queryset.order_by('date_time')
    
    
    #
    # Done.
    #
    return queryset

def report_parse_timezone(request_QUERY_PARAMS, name):
    timezone_string = None
    
    timezone_string = request_QUERY_PARAMS.get(name, None)
    if timezone_string is None:
        content = {'detail': 'Missing time zone parameter {0}.'.format(name)}
        return (None, content)

    if timezone_string not in supported_timezones_set:
        content = {'detail': 'Parameter {0} is not a supported time zone.'.format(name)}
        return (None, content)
    
    return (timezone_string, None)

def report_parse_date(request_QUERY_PARAMS, name):
    date_object = None
    
    date_string = request_QUERY_PARAMS.get(name, None)
    if date_string is None:
        content = {'detail': 'Missing date-time parameter {0}.'.format(name)}
        return (None, content)

    #
    # Parse_datetime raises ValueError if the input is well-formatted but not a valid datetime.
    # It returns None if the input is not well formatted.
    #
    try:
        date_object = parse_datetime(date_string)
    except ValueError:
        content = {'detail': 'Parameter {0} is well-formatted but not a valid date-time.'.format(name)}
        return (None, content)
        
    if date_object is None:
        content = {'detail': 'Parameter {0} is not a well-formatted date-time.'.format(name)}
        return (None, content)
    
    if is_naive(date_object):
        content = {'detail': 'Parameter {0} is a valid date-time but does not specify a time zone.'.format(name)}
        return (None, content)
    
    return (date_object, None)

class DailyRevenueReportOnePlace(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, place_pk=None):
        #
        # Returns report for property with id place_pk.
        #
        
        #
        # IsAuthenticated permission already verifies if the user is logged in,
        # we don't repeat the check.
        #
        
        #
        # Make sure we have a user id for the query.
        #
        user_id = None
        try:
            user_id = request.user.id
        except NameError:
            #
            # No user id was retrieved.
            #
            content = {'detail': 'Authentication error'}
            return Response(content, status=status.HTTP_404_NOT_FOUND)
            
        if user_id is None:
            #
            # No user id was supplied.
            #
            content = {'detail': 'Authentication error'}
            return Response(content, status=status.HTTP_404_NOT_FOUND)
        
        #
        # Make sure we have a place_pk for the query.
        #
        if place_pk is None:
            #
            # No place_id was supplied.
            #
            content = {'detail': 'Could not retrieve property identifier'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        
        #
        # Validate date_to.
        #
        (date_from, error_content) = report_parse_date(request.QUERY_PARAMS, 'date_from')
        if date_from is None:
            return Response(error_content, status=status.HTTP_400_BAD_REQUEST)
        
        #
        # Validate date_from.
        #
        (date_to, error_content) = report_parse_date(request.QUERY_PARAMS, 'date_to')
        if date_to is None:
            return Response(error_content, status=status.HTTP_400_BAD_REQUEST)
        
        #
        # date_from should be strictly smaller than date_to.
        #
        if date_from >= date_to:
            content = {'detail': 'date_from should be strictly before date_to'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        
        #
        # Verify timezone.
        #
        (request_timezone, error_content) = report_parse_timezone(request.QUERY_PARAMS, 'time_zone')
        if request_timezone is None:
            return Response(error_content, status=status.HTTP_400_BAD_REQUEST)
        
        #
        # Ownership check happens during the next check.
        # We don't repeat it here.
        #
        
        #
        # Existence and ownership check.
        # Query the single place with id place_pk and owned by user.
        # Try get() and catch DoesNotExist if data from property is needed, otherwise use exists().
        #
        if not Place.objects.filter(pk=place_pk, user=user_id).exists():
            #
            # No matching place in database.
            #
            content = {'detail': 'Not found'}
            return Response(content, status=status.HTTP_404_NOT_FOUND)
        
        #
        # Get query set for all reservations associated with this place and user.
        #
        # queryset = PlaceReservation.objects.filter(place=place_pk, user=user_id)
        
        #
        # Set up custom query to get report queryset.
        #
        # queryset = get_report_queryset(queryset, request_timezone)
        
        rows = get_daily_report_tuples(time_zone=request_timezone,
                                       place_id=place_pk,
                                       user_id=user_id,
                                       date_from=date_from,
                                       date_to=date_to)
        
        #
        # We assume the right renderer and serializer will be selected.
        # There might be a bug.
        #
        return Response(rows)
