from django.conf.urls import url, include
from rest_framework import routers
from .views.place_reservations_views import PlaceViewSet, PlaceReservationViewSet
from .views.reports import DailyRevenueReportOnePlace

# Router for Places and Reservations REST API endpoints.
router = routers.DefaultRouter()
router.register(r'places', PlaceViewSet, base_name='place')
router.register(r'reservations', PlaceReservationViewSet, base_name='reservation')

# Urls for reports REST API read-only endpoints. Example url:
# http://server/places/5454545/reports/revenue/daily/one/123456/?start_date=...&end_date=...&time_zone=...&locale=...
daily_report_patterns = [
    url(r'^one/(?P<place_pk>[^/]+)/$', DailyRevenueReportOnePlace.as_view(), name='daily-revenue-report-one-place'),
#    url(r'^all/$', DailyRevenueReportAllPlaces.as_view(), name='daily-revenue-report-all-places'),
]

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^reports/revenue/daily/', include(daily_report_patterns)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
