from places.models import Place
from place_reservations.models import PlaceReservation
from rest_framework import serializers
from rest_framework.exceptions import ParseError
from rest_framework.reverse import reverse

# Utility to return the full url of a report.
def get_report_full_url(view_name, place_pk, request):
    
    # Get full report url for given place_pk.
    full_report_url = reverse(view_name, args=[place_pk], request=request)
    
    return full_report_url

class PlaceSerializer(serializers.HyperlinkedModelSerializer):
    # Read-only property pointing to a place-specific report.
    daily_revenue_report_url = serializers.SerializerMethodField('get_daily_revenue_report_full_url')
    
    def get_daily_revenue_report_full_url(self, place):
        view_name = 'daily-revenue-report-one-place'
        request = self.get_request()
        return get_report_full_url(view_name, place.id, request)
    
    def get_request(self):
        # Get the full report root url.
        # BUGBUG. Add logging code for this error.
        request = self.context.get('request', None)
        if request is None:
            raise ParseError('Invalid request.')
        return request
    
    class Meta:
        model = Place
        fields = ('name', 'description', 'currency', 'date_created', 'date_modified', 'daily_revenue_report_url', 'url')

class PlaceReservationSerializer(serializers.HyperlinkedModelSerializer):
    # Attempt at access control. Restrict the list of places displayed
    # when creating a new PlaceReservation.
    def get_fields(self, *args, **kwargs):
        fields = super(PlaceReservationSerializer, self).get_fields(*args, **kwargs)
        fields['place'].queryset = fields['place'].queryset.filter(user_id=self.context['view'].request.user.id)
        return fields
    
    # Price is a decimal number and must be positive
    def validate_price(self, attrs, source):
        value = attrs[source]
        if value.is_signed():
            raise serializers.ValidationError("Price must be positive.")
        return attrs
    
    class Meta:
        model = PlaceReservation
        view_name = 'reservation-detail'
        fields = ('name', 'description', 'price', 'date_from', 'date_to', 'place', 'url')
