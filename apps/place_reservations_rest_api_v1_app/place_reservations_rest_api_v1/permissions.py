from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to read/update/delete it.
    Assumes the model instance has a `user` attribute.
    Object-level permissions are not used by generic views for lists of objects. In this case
    we assume the get_queryset() method of the view returns the proper list.
    """

    def has_object_permission(self, request, view, obj):
        # Instance must have an attribute named `user`.
        return (obj.user_id == request.user.id)
