from django.conf.urls import patterns
from django.views.generic import TemplateView

urlpatterns = patterns('django.contrib.auth.views',
    (r'^$', TemplateView.as_view(template_name="sail_dummy_app/dummy_app.html")),
)
