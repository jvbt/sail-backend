"""
Use sail_frontend_app as the namespace when including those urls inside another urlpatterns:

    urlpatterns = patterns('',
        ...
        url(r'^app/', include('sail_frontend_app.urls', namespace='sail_frontend_app'))
    )
"""

from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns('django.contrib.auth.views',
    url(r'^login/$', 'login', {'template_name': 'sail_frontend_app/login.html', 'redirect_field_name': None}, name='login'),
    url(r'^logout/$', 'logout', {'template_name': 'sail_frontend_app/logout.html', 'redirect_field_name':None}, name='logout'),
    (r'^$', TemplateView.as_view(template_name="sail_frontend_app/index.html")),
)
